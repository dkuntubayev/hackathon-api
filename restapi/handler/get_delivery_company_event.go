package handler

import (
	"github.com/gorilla/mux"
	"hackathon/repository/sqlrepo"
	"hackathon/restapi/response"
	"net/http"
)

func GetDeliveryCompanyEvents(repo *sqlrepo.SQLRepo, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	deliveryCompanyId, ok := vars["delivery_company_id"]
	if !ok {
		respondError(w, http.StatusBadRequest, "delivery company id is missing in parameters")
	}
	filter := map[string]interface{}{
		"delivery_company_id": deliveryCompanyId,
	}
	events, err := repo.GetEvents(filter)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
	}
	locations, err := repo.GetLocations(nil)
	locationsMap := map[uint64]response.LocationResp{}
	for _, l := range locations {
		locationsMap[l.Id] = response.LocationResp{
			Name:      l.Name,
			Longitude: l.Longitude,
			Latitude:  l.Latitude,
		}
	}
	resp := []response.EventResp{}
	for _, e := range events {
		reason := e.FailReason
		if reason == "" {
			reason = e.CancelReason
		}
		eventResp := response.EventResp{
			Id: e.Id,
			EventTypeId: e.EventTypeId,
			ClientId: e.ClientId,
			FromLocation: locationsMap[e.FromLocationId],
			ToLocation: locationsMap[e.ToLocationId],
			WeightKg: e.WeightKg,
			ExpectingDeliveryDate: e.ExpectingDeliveryDate,
			Date: e.Date,
			OfferId: e.OfferId,
			DeliveryCompanyId: e.DeliveryCompanyId,
			OfferedPriceTenge: e.OfferedPriceTenge,
			Reason: reason,
		}
		resp = append(resp, eventResp)
	}
	respondJSON(w, http.StatusOK, resp)
}