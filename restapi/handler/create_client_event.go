package handler

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"hackathon/repository/sqlrepo"
	"hackathon/restapi/request"
	"hackathon/restapi/response"
	"hackathon/services"
	"net/http"
)

func CreateClientEvent(repo *sqlrepo.SQLRepo, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	clientId, ok := vars["client_id"]
	if !ok {
		respondError(w, http.StatusBadRequest, "client id is missing in parameters")
	}
	re := request.RequestEvent{}
	err := json.NewDecoder(r.Body).Decode(&re)
	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
	}
	fromLocationClass, toLocationClass := services.EventsClassificationByLocation(re)
	distance := services.DistanceIdentification(re)
	offers := services.OfferIdentification(re, fromLocationClass, toLocationClass, distance, clientId)
	err = repo.CreateOffers(offers)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
	}
	fl := response.LocationResp{
		Name:      re.FromLocation.Name,
		Longitude: re.FromLocation.Longitude,
		Latitude:  re.FromLocation.Latitude,
	}
	tl := response.LocationResp{
		Name:      re.ToLocation.Name,
		Longitude: re.ToLocation.Longitude,
		Latitude:  re.ToLocation.Latitude,
	}
	resp := response.EventResp{
		Id: 1,
		EventTypeId: re.EventTypeId,
		ClientId:    re.ClientId,
		FromLocation: fl,
		ToLocation: tl,
		WeightKg: re.WeightKg,
		ExpectingDeliveryDate: re.ExpectingDeliveryDate,
		Date: re.Date,
		OfferId: re.OfferId,
		DeliveryCompanyId: re.DeliveryCompanyId,
		OfferedPriceTenge: re.OfferedPriceTenge,
		Reason: re.Reason,
	}
	respondJSON(w, http.StatusOK, resp)
}
