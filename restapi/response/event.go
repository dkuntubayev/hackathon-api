package response

import "time"

type LocationResp struct {
	Name      string  `json:"name"`
	Longitude float32 `json:"longitude"`
	Latitude  float32 `json:"latitude"`
}

type EventResp struct {
	Id                    uint64       `json:"id"`
	EventTypeId           uint         `json:"event_type_id"`
	ClientId              int          `json:"client_id"`
	FromLocation          LocationResp `json:"from_location"`
	ToLocation            LocationResp `json:"to_location"`
	WeightKg              float32      `json:"weight_kg"`
	ExpectingDeliveryDate time.Time    `json:"expecting_delivery_date"`
	Date                  time.Time    `json:"date"`
	OfferId               uint64       `json:"offer_id"`
	DeliveryCompanyId     uint64       `json:"delivery_company_id"`
	OfferedPriceTenge     int          `json:"offered_price_tenge"`
	Reason                string       `json:"reason"`
}
