package app

import (
	"database/sql"
	"github.com/gorilla/mux"
	"hackathon/config"
	"hackathon/repository/sqlrepo"
	"hackathon/restapi/handler"
	"log"
	"net/http"
)

type App struct {
	Router *mux.Router
	Repository *sqlrepo.SQLRepo
}

func (app *App) Initialize(config *config.Config) error {
	dsn := config.DB.DataSource.FormatDSN()
	db, err := sql.Open(config.DB.Driver, dsn)
	app.Repository, err = sqlrepo.New(db)
	if err != nil {
		return err
	}
	app.Router = mux.NewRouter()
	app.registerRouters()
	return nil
}

func (app * App) registerRouters() {
	app.Post("/clients/{client_id}/events", app.handleRequest(handler.CreateClientEvent))
	app.Get("/clients/{client_id}/events", app.handleRequest(handler.GetClientEvents))
	app.Post("/delivery_company/{delivery_company_id}/events", app.handleRequest(handler.CreateDeliveryCompanyEvent))
	app.Get("/delivery_company/{delivery_company_id}/events", app.handleRequest(handler.GetDeliveryCompanyEvents))

}

func (app *App) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	app.Router.HandleFunc(path, f).Methods(http.MethodGet)
}

func (app *App) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	app.Router.HandleFunc(path, f).Methods(http.MethodPost)
}

type RequestHandlerFunction func(repo *sqlrepo.SQLRepo, w http.ResponseWriter, r *http.Request)

func (app *App) handleRequest(handler RequestHandlerFunction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		handler(app.Repository, w, r)
	}
}

func (app *App) Run(host string) {
	log.Fatal(http.ListenAndServe(host, app.Router))
}
