CREATE TABLE data.customer_profiles (
    id bigserial NOT NULL,
    name varchar NOT NULL,
    date timestamp NOT NULL DEFAULT NOW(),
    CONSTRAINT customer_profiles_pk PRIMARY KEY (id)
);