#!/bin/bash

set -e
set -u

BASEDIR=$(dirname "$0")
echo "Current directory: '${BASEDIR}'"

function create_database() {
  local database=$1
  echo "  Creating database '$database'"
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	    CREATE DATABASE $database;
	    GRANT ALL PRIVILEGES ON DATABASE $database TO $POSTGRES_USER;
EOSQL
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -d "${database}" <<-EOSQL
      CREATE SCHEMA partman;
      CREATE EXTENSION pg_partman WITH SCHEMA partman;
EOSQL
  local scripts=$2
  for script in $(echo "$scripts" | tr ',' ' '); do
    echo "  Running script '$script'"
    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -d "${database}" -f "${BASEDIR}/${script}"
  done
}

if [ -n "$POSTGRES_MULTIPLE_DATABASES" ]; then
  echo "Multiple database creation requested: $POSTGRES_MULTIPLE_DATABASES"
  for db in $(echo "$POSTGRES_MULTIPLE_DATABASES" | tr ',' ' '); do
    dbname="${db}_NAME"
    dbscripts="${db}_SCRIPTS"
    create_database "${!dbname}" "${!dbscripts}"
  done
  echo "Multiple databases created"
fi

echo "ADDING pg_partman_bgw TO postgresql.conf"
{
  echo "shared_preload_libraries = 'pg_cron,pg_partman_bgw'"
  echo "pg_partman_bgw.interval = 3600"
  echo "pg_partman_bgw.role = '$POSTGRES_USER'"
if [ -n "$POSTGRES_MULTIPLE_DATABASES" ]; then
  dbnames=""
  for db in $(echo "$POSTGRES_MULTIPLE_DATABASES" | tr ',' ' '); do
    dbname="${db}_NAME"
    dbnames+="${!dbname},"
  done
  dbnames="${dbnames%,}"
  echo "pg_partman_bgw.dbname = '$dbnames'"
  echo "cron.database_name = 'postgres'"
fi
} >> "$PGDATA"/postgresql.conf