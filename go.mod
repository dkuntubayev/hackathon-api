module hackathon

go 1.17

require (
	github.com/Masterminds/squirrel v1.5.2
	github.com/golang-basic/golearn v0.0.0-20141106095017-ec846e62d2ef
	github.com/gorilla/mux v1.8.0
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0
)

require (
	github.com/gonum/blas v0.0.0-20181208220705-f22b278b28ac // indirect
	github.com/gonum/floats v0.0.0-20181209220543-c233463c7e82 // indirect
	github.com/gonum/internal v0.0.0-20181124074243-f884aa714029 // indirect
	github.com/gonum/lapack v0.0.0-20181123203213-e4cdc5a0bff9 // indirect
	github.com/gonum/matrix v0.0.0-20181209220409-c518dec07be9 // indirect
	github.com/google/go-cmp v0.4.0 // indirect
	github.com/guptarohit/asciigraph v0.5.1 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/mattn/go-runewidth v0.0.7 // indirect
	github.com/olekukonko/tablewriter v0.0.4 // indirect
	github.com/rocketlaunchr/dataframe-go v0.0.0-20201007021539-67b046771f0b // indirect
	github.com/sjwhitworth/golearn v0.0.0-20211014193759-a8b69c276cd8 // indirect
	github.com/smartystreets/goconvey v1.7.2 // indirect
	golang.org/x/exp v0.0.0-20200331195152-e8c3332aa8e5 // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	gonum.org/v1/gonum v0.8.1 // indirect
)
