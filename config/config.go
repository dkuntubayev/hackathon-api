package config

import "fmt"

type Config struct {
	DB  *DBConfig
	API *APIConfig
}

type DBConfig struct {
	Driver     string
	DataSource *DataSource
}

type DataSource struct {
	Name     string
	Host     string
	Port     int
	Username string
	Password string
}

func (ds *DataSource) FormatDSN() string {
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		ds.Host, ds.Port, ds.Username, ds.Password, ds.Name)
	return dsn
}

type APIConfig struct {
	Port string
}

func GetConfig() *Config {
	return &Config{
		DB: &DBConfig{
			Driver: "postgres",
			DataSource: &DataSource{
				Name:     "hackathon_db",
				Host:     "localhost",
				Port:     5432,
				Username: "docker",
				Password: "docker",
			},
		},
		API: &APIConfig{
			Port: ":8081",
		},
	}
}
