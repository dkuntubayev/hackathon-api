package main

import (
	"hackathon/cmd/app"
	"hackathon/config"
	"log"
)

func main() {
	config := config.GetConfig()
	app := &app.App{}
	err := app.Initialize(config)
	if err != nil {
		log.Fatalf("Could not initialize the app: err=%s", err.Error())
	}
	app.Run(config.API.Port)
}
