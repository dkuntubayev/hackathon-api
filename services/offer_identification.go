package services

import (
	"hackathon/repository/repomodel"
	"hackathon/restapi/request"
	"strconv"
	"time"
)

func OfferIdentification(re request.RequestEvent, flc, tlc int, dist float32, clientId string) []repomodel.Offer {
	clID, _ := strconv.Atoi(clientId)
	return []repomodel.Offer{
		repomodel.Offer{
			ClientId: clID,
			OfferId: 1,
			EventId: 22,
			DeliveryCompanyId: 3,
			StartDate: time.Now(),
			DueDate: time.Now().AddDate(0, 0, 1),
			PriceTg: 100000,
		},
		repomodel.Offer{
			OfferId: 2,
			EventId: 22,
			DeliveryCompanyId: 7,
			StartDate: time.Now(),
			DueDate: time.Now().AddDate(0, 0, 4),
			PriceTg: 60000,
		},

	}
}
