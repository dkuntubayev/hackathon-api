package repomodel

import "time"

type Client struct {
	Id   int
	Name string
	Date time.Time
}
