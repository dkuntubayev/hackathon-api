package repomodel

import "time"

type Offer struct {
	ClientId int
	OfferId uint64
	EventId uint64
	DeliveryCompanyId uint64
	StartDate time.Time
	DueDate time.Time
	PriceTg int
}
