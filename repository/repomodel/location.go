package repomodel

type Location struct {
	Id uint64
	Name string
	Longitude float32
	Latitude float32
}