package repomodel

import "time"

type DeliveryCompany struct {
	Id 		uint64
	Name	string
	Date	time.Time
}
