package repomodel

import "time"

type Event struct {
	Id                    uint64
	EventTypeId           uint
	ClientId              int
	FromLocationId        uint64
	ToLocationId          uint64
	WeightKg              float32
	ExpectingDeliveryDate time.Time
	Date                  time.Time
	OrderId               uint64
	OfferId               uint64
	DeliveryCompanyId     uint64
	OfferedPriceTenge     int
	CancelReason          string
	FailReason            string
}
