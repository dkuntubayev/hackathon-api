package repository

import "errors"

var (
	// ErrNotFound returned when there are no rows in result set
	ErrNotFound = errors.New("repository: not found")

	// ErrAlreadyExists returned when row with given criteria already exists
	ErrAlreadyExists = errors.New("repository: already exists")

	// ErrAlreadySoftDeleted returned when row with given criteria already soft deleted
	ErrAlreadySoftDeleted = errors.New("repository: already deleted")
)
