package sqlrepo

import (
	"hackathon/repository/repomodel"
	eventstable "hackathon/repository/sqlrepo/tables/events"
)

func (r *SQLRepo) GetEvents(filter map[string]interface{}) ([]repomodel.Event, error) {
	// Query DB
	rows, err := r.db.
		Select(
		eventstable.ColumnID,
		eventstable.ColumnEventTypeID,
		eventstable.ColumnClient,
		eventstable.ColumnFromLocationId,
		eventstable.ColumnToLocationId,
		eventstable.ColumnWeightKg,
		eventstable.ColumnExpectingDeliveryDate,
		eventstable.ColumnDate,
		eventstable.ColumnOrderId,
		eventstable.ColumnOfferId,
		eventstable.ColumnDeliveryCompanyId,
		eventstable.ColumnOfferedPriceTenge,
		eventstable.ColumnCancelReason,
		eventstable.ColumnFailReason,
		).
		From(eventstable.TableName).Where(filter).Query()
	if err != nil {
		return nil, wrapSQLErrors(err)
	}

	var results []repomodel.Event

	for rows.Next() {
		var e repomodel.Event

		err := rows.Scan(
			&e.Id,
			&e.EventTypeId,
			&e.ClientId,
			&e.FromLocationId,
			&e.ToLocationId,
			&e.WeightKg,
			&e.ExpectingDeliveryDate,
			&e.Date,
			&e.OrderId,
			&e.OfferId,
			&e.DeliveryCompanyId,
			&e.OfferedPriceTenge,
			&e.CancelReason,
			&e.FailReason,
		)
		if err != nil {
			return nil, err
		}

		results = append(results, e)
	}

	return results, nil
}