package events

// Table name and column names
const (
	TableName                   = "data.events"
	ColumnID                    = "id"
	ColumnEventTypeID           = "event_type_id"
	ColumnClient                = "client_id"
	ColumnFromLocationId        = "from_location_id"
	ColumnToLocationId          = "to_location_id"
	ColumnWeightKg              = "weight_kg"
	ColumnExpectingDeliveryDate = "expecting_delivery_date"
	ColumnDate                  = "date"
	ColumnOrderId               = "order_id"
	ColumnOfferId               = "offer_id"
	ColumnDeliveryCompanyId     = "delivery_company_id"
	ColumnOfferedPriceTenge     = "offered_price_tenge"
	ColumnCancelReason          = "cancel_reason"
	ColumnFailReason            = "fail_reason"
)
