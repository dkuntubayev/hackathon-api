package offers

const (
	TableName               = "data.offers"
	ColumnClientID          = "client_id"
	ColumnOfferID           = "offer_id"
	ColumnEventID           = "event"
	ColumnDeliveryCompanyId = "delivery_company_id"
	ColumnStartDate         = "start_date"
	ColumnDueDate           = "due_date"
	ColumnPriceTg           = "price_tg"
)
