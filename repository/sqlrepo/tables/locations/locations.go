package locations

const (
	TableName          = "data.locations"
	ColumnID           = "id"
	ColumnName         = "name"
	ColumnLongitude    = "longitude"
	ColumnLatitude = "latitude"
)
