package sqlrepo

import (
	"hackathon/repository/repomodel"
	offerstable "hackathon/repository/sqlrepo/tables/offers"
)

func (r *SQLRepo) CreateOffers(offers []repomodel.Offer) error {
	// Query DB
	for _, o := range offers {
		var offer repomodel.Offer
		if err := r.db.
			Insert(offerstable.TableName).
			Columns(
				offerstable.ColumnClientID,
				offerstable.ColumnOfferID,
				offerstable.ColumnEventID,
				offerstable.ColumnDeliveryCompanyId,
				offerstable.ColumnStartDate,
				offerstable.ColumnDueDate,
				offerstable.ColumnPriceTg,
			).
			Values(
				o.ClientId,
				o.OfferId,
				o.EventId,
				o.DeliveryCompanyId,
				o.StartDate,
				o.DueDate,
				o.PriceTg,
			).
			Scan(
				&offer.ClientId,
				&offer.OfferId,
				&offer.EventId,
				&offer.DeliveryCompanyId,
				&offer.StartDate,
				&offer.DueDate,
				&offer.PriceTg,
			); err != nil {
			return wrapSQLErrors(err)
			}
	}
	return nil
}
