package sqlrepo

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	"github.com/lann/builder"
	"hackathon/repository"
)

// New creates new repository using SQL as backend
func New(db *sql.DB,) (*SQLRepo, error) {

	return &SQLRepo{
		db: sq.
			StatementBuilderType(builder.EmptyBuilder).
			PlaceholderFormat(sq.Dollar).
			RunWith(sq.NewStmtCacher(db)),
	}, nil
}

// SQLRepo is a repository using SQL as backend
type SQLRepo struct {
	db         sq.StatementBuilderType
}

func wrapSQLErrors(err error) error {
	if err == sql.ErrNoRows {
		return repository.ErrNotFound
	}
	return err
}
