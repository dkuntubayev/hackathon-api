package sqlrepo

import (
	"hackathon/repository/repomodel"
	locationstable "hackathon/repository/sqlrepo/tables/locations"
)

func (r *SQLRepo) GetLocations(filter map[string]interface{}) ([]repomodel.Location, error) {
	// Query DB
	rows, err := r.db.
		Select(
			locationstable.ColumnID,
			locationstable.ColumnName,
			locationstable.ColumnLongitude,
			locationstable.ColumnLatitude,
		).
		From(locationstable.TableName).Where(filter).Query()
	if err != nil {
		return nil, wrapSQLErrors(err)
	}

	var results []repomodel.Location

	for rows.Next() {
		var l repomodel.Location

		err := rows.Scan(
			&l.Id,
			&l.Name,
			&l.Longitude,
			&l.Latitude,
		)
		if err != nil {
			return nil, err
		}

		results = append(results, l)
	}

	return results, nil
}
